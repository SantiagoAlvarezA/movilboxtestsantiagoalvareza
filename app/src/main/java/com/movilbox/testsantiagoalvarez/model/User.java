package com.movilbox.testsantiagoalvarez.model;

import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.Index;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(indices = {@Index("id"), @Index("address_zipcode"), @Index("company_name")},
        primaryKeys = {"id", "address_zipcode", "company_name"}
)
public class User implements Parcelable {

    @SerializedName("id")
    @Expose
    @NonNull
    public Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("address")
    @Expose
    @Embedded(prefix = "address_")
    @NonNull
    private Address address;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("website")
    @Expose
    private String website;
    @SerializedName("company")
    @Expose
    @Embedded(prefix = "company_")
    @NonNull
    private Company company;


    public final static Creator<User> CREATOR = new Creator<User>() {


        @SuppressWarnings({
                "unchecked"
        })
        public User createFromParcel(android.os.Parcel in) {
            return new User(in);
        }

        public User[] newArray(int size) {
            return (new User[size]);
        }

    };
    private final static long serialVersionUID = -1318856395609465244L;

    protected User(android.os.Parcel in) {
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        this.username = ((String) in.readValue((String.class.getClassLoader())));
        this.email = ((String) in.readValue((String.class.getClassLoader())));
        this.address = ((Address) in.readValue((Address.class.getClassLoader())));
        this.phone = ((String) in.readValue((String.class.getClassLoader())));
        this.website = ((String) in.readValue((String.class.getClassLoader())));
        this.company = ((Company) in.readValue((Company.class.getClassLoader())));
    }


    /**
     * @param website
     * @param address
     * @param phone
     * @param name
     * @param company
     * @param id
     * @param email
     * @param username
     */
    public User(Integer id, String name, String username, String email, Address address, String phone, String website, Company company) {
        super();
        this.id = id;
        this.name = name;
        this.username = username;
        this.email = email;
        this.address = address;
        this.phone = phone;
        this.website = website;
        this.company = company;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(name);
        dest.writeValue(username);
        dest.writeValue(email);
        dest.writeValue(address);
        dest.writeValue(phone);
        dest.writeValue(website);
        dest.writeValue(company);
    }

    public int describeContents() {
        return 0;
    }


    public static class Company {

        @SerializedName("name")
        @NonNull
        public final String name;
        @SerializedName("catchPhrase")
        public final String catchPhrase;
        @SerializedName("bs")
        public final String bs;

        public Company(@NonNull String name, String catchPhrase, String bs) {
            this.name = name;
            this.catchPhrase = catchPhrase;
            this.bs = bs;
        }


    }

    @Entity(
            indices = {@Index("zipcode"), @Index("geo_id")},
            primaryKeys = {"zipcode", "geo_id"}
    )
    public static class Address {

        @SerializedName("street")
        public final String street;
        @SerializedName("suite")
        public final String suite;
        @SerializedName("city")
        public final String city;
        @SerializedName("zipcode")
        @NonNull
        public final String zipcode;
        @SerializedName("geo")
        @Embedded(prefix = "geo_")
        @NonNull
        public final Geo geo;


        public Address(String street, String suite, String city, @NonNull String zipcode, Geo geo) {
            this.street = street;
            this.suite = suite;
            this.city = city;
            this.zipcode = zipcode;
            this.geo = geo;
        }


        public static class Geo {

            public static final int UNKNOWN_ID = -1;

            public final int id;
            @SerializedName("lat")
            @Expose
            public final String lat;
            @SerializedName("lng")
            @Expose
            public final String lng;

            public Geo(int id, String lat, String lng) {
                this.id = id;
                this.lat = lat;
                this.lng = lng;
            }


        }
    }

}