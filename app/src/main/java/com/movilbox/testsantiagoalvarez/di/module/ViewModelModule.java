package com.movilbox.testsantiagoalvarez.di.module;

import androidx.lifecycle.ViewModel;

import com.movilbox.testsantiagoalvarez.repository.PostRepository;
import com.movilbox.testsantiagoalvarez.repository.UserRepository;
import com.movilbox.testsantiagoalvarez.ui.detail.MapDetailViewModel;
import com.movilbox.testsantiagoalvarez.ui.home.HomeViewModel;
import com.movilbox.testsantiagoalvarez.viewmodel.ViewModelFactory;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Map;

import javax.inject.Provider;

import dagger.MapKey;
import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.components.SingletonComponent;
import dagger.multibindings.IntoMap;

@Module
@InstallIn(SingletonComponent.class)
public class ViewModelModule {


    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.RUNTIME)
    @MapKey
    @interface ViewModelKey {
        Class<? extends ViewModel> value();
    }

    @Provides
    ViewModelFactory viewModelFactory(Map<Class<? extends ViewModel>, Provider<ViewModel>> providerMap) {
        return new ViewModelFactory(providerMap);
    }

    @Provides
    @IntoMap
    @ViewModelKey(HomeViewModel.class)
    ViewModel viewModelHome(PostRepository postRepository) {
        return new HomeViewModel(postRepository);
    }
    @Provides
    @IntoMap
    @ViewModelKey(MapDetailViewModel.class)
    ViewModel MapDetailViewModel(UserRepository postRepository) {
        return new MapDetailViewModel(postRepository);
    }
}