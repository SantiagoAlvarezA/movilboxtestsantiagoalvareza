package com.movilbox.testsantiagoalvarez.di;

import android.app.Application;

import androidx.room.Room;

import com.movilbox.testsantiagoalvarez.BuildConfig;
import com.movilbox.testsantiagoalvarez.api.WebServiceApi;
import com.movilbox.testsantiagoalvarez.db.MBoxDb;
import com.movilbox.testsantiagoalvarez.db.PostDao;
import com.movilbox.testsantiagoalvarez.db.UserDao;
import com.movilbox.testsantiagoalvarez.utils.LiveDataCallAdapterFactory;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.components.SingletonComponent;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
@InstallIn(SingletonComponent.class)
public class AppModule {


    @Singleton
    @Provides
    WebServiceApi provideWebServiceApi() {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.URL_API)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(new LiveDataCallAdapterFactory())
                .build()
                .create(WebServiceApi.class);
    }

    @Singleton
    @Provides
    MBoxDb provideDb(Application app) {
        return Room.databaseBuilder(app, MBoxDb.class, "MBox.db").build();
    }

    @Singleton
    @Provides
    PostDao providePostDao(MBoxDb db) {
        return db.postDao();
    }

    @Singleton
    @Provides
    UserDao provideUserDao(MBoxDb db) {
        return db.userDao();
    }
}
