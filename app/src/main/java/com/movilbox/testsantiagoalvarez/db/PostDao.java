package com.movilbox.testsantiagoalvarez.db;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import com.movilbox.testsantiagoalvarez.model.Post;

import java.util.List;

@Dao
public abstract class PostDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    protected abstract void insert(List<Post> posts);

    @Query("SELECT * FROM Post WHERE id = :id")
    public abstract LiveData<Post> getPostById(Integer id);

    @Query("SELECT * FROM Post WHERE userId = :userId")
    public abstract LiveData<List<Post>> getAllPostByUser(Integer userId);

    @Query("SELECT * FROM Post WHERE `like`= :filter")
    public abstract LiveData<List<Post>> getAllPostFilter(boolean filter);

    @Query("SELECT * FROM Post")
    public abstract LiveData<List<Post>> getAllPost();


    @Query("UPDATE Post SET `like` = :unlike WHERE id = :id")
    public abstract void likeChange(boolean unlike, Integer id);

    @Query("UPDATE Post SET unread = 0 WHERE id = :id")
    public abstract void setRead(Integer id);

    @Update()
    public abstract void update(Post... posts);

    @Delete()
    public abstract void delete(Post... posts);

    @Query("DELETE FROM post")
    public abstract void deleteAll();

    @Transaction
    public void insertAll(List<Post> posts){

        if (posts!=null && posts.size()>20){
            for (int pos = 0; pos<20 ;pos++ ){
                posts.get(pos).setUnread(true);
            }
        }else if (posts != null && posts.size()>1){
            for (int pos = 0; pos<posts.size()/2 ;pos++ ){
                posts.get(pos).setUnread(true);
            }
        }

        insert(posts);


    }
}
