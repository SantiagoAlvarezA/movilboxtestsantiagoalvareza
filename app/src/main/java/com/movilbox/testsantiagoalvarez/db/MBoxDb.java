package com.movilbox.testsantiagoalvarez.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.movilbox.testsantiagoalvarez.model.Post;
import com.movilbox.testsantiagoalvarez.model.User;

@Database(entities = {User.class, Post.class}, version = 1)
public abstract class MBoxDb extends RoomDatabase {

    public abstract UserDao userDao();

    public abstract PostDao postDao();
}
