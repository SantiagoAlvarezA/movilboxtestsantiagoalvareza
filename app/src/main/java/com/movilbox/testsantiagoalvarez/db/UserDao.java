package com.movilbox.testsantiagoalvarez.db;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.movilbox.testsantiagoalvarez.model.User;

@Dao
public abstract class UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insert(User... users);

    @Query("SELECT * FROM User WHERE id = :id")
    public abstract LiveData<User> getUserById(Integer id);

    @Update()
    public abstract void updateUser(User user);
}
