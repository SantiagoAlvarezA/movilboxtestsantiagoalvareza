package com.movilbox.testsantiagoalvarez;


import android.app.Application;

import dagger.hilt.android.HiltAndroidApp;

@HiltAndroidApp
public class MyApplication  extends Application {
}
