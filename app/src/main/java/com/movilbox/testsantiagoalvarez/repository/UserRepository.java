package com.movilbox.testsantiagoalvarez.repository;

import androidx.lifecycle.LiveData;

import com.movilbox.testsantiagoalvarez.AppExecutors;
import com.movilbox.testsantiagoalvarez.api.ApiResponse;
import com.movilbox.testsantiagoalvarez.api.WebServiceApi;
import com.movilbox.testsantiagoalvarez.db.MBoxDb;
import com.movilbox.testsantiagoalvarez.db.UserDao;
import com.movilbox.testsantiagoalvarez.model.User;
import com.movilbox.testsantiagoalvarez.utils.RateLimiter;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class UserRepository {

    private final MBoxDb db;
    private final UserDao userDao;
    private final WebServiceApi webServiceApi;
    private final AppExecutors appExecutors;

    private final RateLimiter<String> repoListRateLimit = new RateLimiter<>(10, TimeUnit.MINUTES);

    @Inject
    public UserRepository(MBoxDb db, UserDao userDao, WebServiceApi webServiceApi, AppExecutors appExecutors) {
        this.db = db;
        this.userDao = userDao;
        this.webServiceApi = webServiceApi;
        this.appExecutors = appExecutors;
    }

/*    public void getUserById(Integer userId, MutableLiveData<User> user) {
        Call<User> call = webServiceApi.getUserById(userId);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(@NotNull Call<User> call,
                                   @NotNull Response<User> response) {
                if (response.isSuccessful()) {
                    user.postValue(response.body());
                } else {
                    user.postValue(null);
                }

            }

            @Override
            public void onFailure(@NotNull Call<User> call, @NotNull Throwable t) {
                user.postValue(null);
            }
        });
    }*/

    public LiveData<Resource<User>> getUserById(Integer userId) {

        return new NetworkBoundResource<User, User>(appExecutors) {
            @Override
            protected boolean shouldFetch(User data) {
                return data == null || repoListRateLimit.shouldFetch(String.valueOf(userId));
            }

            @Override
            protected LiveData<User> loadFromDb() {
                return userDao.getUserById(userId);
            }

            @Override
            protected void saveCallResult(User item) {
                userDao.insert(item);
            }

            @Override
            protected LiveData<ApiResponse<User>> createCall() {
                return webServiceApi.getUserById(userId);
            }

            @Override
            protected void onFetchFailed() {
                repoListRateLimit.reset(String.valueOf(userId));
            }
        }.asLiveData();


    }


}
