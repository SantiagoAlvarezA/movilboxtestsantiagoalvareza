package com.movilbox.testsantiagoalvarez.repository;

public enum Status{
    LOADING,
    SUCCESS,
    ERROR
}