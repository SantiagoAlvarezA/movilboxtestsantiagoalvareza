package com.movilbox.testsantiagoalvarez.repository;

import androidx.lifecycle.LiveData;

import com.movilbox.testsantiagoalvarez.AppExecutors;
import com.movilbox.testsantiagoalvarez.api.ApiResponse;
import com.movilbox.testsantiagoalvarez.api.WebServiceApi;
import com.movilbox.testsantiagoalvarez.db.MBoxDb;
import com.movilbox.testsantiagoalvarez.db.PostDao;
import com.movilbox.testsantiagoalvarez.model.Post;
import com.movilbox.testsantiagoalvarez.utils.RateLimiter;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

public class PostRepository {

    private final MBoxDb db;
    private final PostDao postDao;
    private final WebServiceApi webServiceApi;
    private final AppExecutors appExecutors;

    private final RateLimiter<String> repoListRateLimit = new RateLimiter<>(10, TimeUnit.MINUTES);


    @Inject
    public PostRepository(MBoxDb db, PostDao postDao, WebServiceApi webServiceApi, AppExecutors appExecutors) {
        this.db = db;
        this.postDao = postDao;
        this.webServiceApi = webServiceApi;
        this.appExecutors = appExecutors;
    }


    public LiveData<Resource<List<Post>>> getAllPost() {

        return new NetworkBoundResource<List<Post>, List<Post>>(appExecutors) {
            @Override
            protected boolean shouldFetch(List<Post> data) {
                return data == null || data.isEmpty() || repoListRateLimit.shouldFetch("Post");
            }

            @Override
            protected LiveData<List<Post>> loadFromDb() {
                return postDao.getAllPost();
            }

            @Override
            protected void saveCallResult(List<Post> posts) {
                postDao.insertAll(posts);
            }

            @Override
            protected LiveData<ApiResponse<List<Post>>> createCall() {
                return webServiceApi.getAllPost();
            }

            @Override
            protected void onFetchFailed() {
                repoListRateLimit.reset("Post");
            }

        }.asLiveData();
    }


    public LiveData<Resource<List<Post>>> getAllPostFilter(boolean filter) {

        return new NetworkBoundResource<List<Post>, List<Post>>(appExecutors) {
            @Override
            protected boolean shouldFetch(List<Post> data) {
                return data == null || data.isEmpty() || repoListRateLimit.shouldFetch("Post");
            }

            @Override
            protected LiveData<List<Post>> loadFromDb() {
                return postDao.getAllPostFilter(filter);
            }

            @Override
            protected void saveCallResult(List<Post> posts) {
                postDao.insertAll(posts);
            }

            @Override
            protected LiveData<ApiResponse<List<Post>>> createCall() {
                return webServiceApi.getAllPost();
            }

            @Override
            protected void onFetchFailed() {
                repoListRateLimit.reset("Post");
            }

        }.asLiveData();
    }


    public void likeUnLikePost(boolean like, Integer id) {
        new Thread(() -> postDao.likeChange(like, id)).start();
    }

    public void setRead(Integer id) {
        new Thread(() -> postDao.setRead(id)).start();
    }

    public void deleteAll() {
        repoListRateLimit.reset("Post");
        new Thread(postDao::deleteAll).start();
    }

    public void deletePost(Post post) {
        new Thread(() -> postDao.delete(post)).start();
    }


}
