package com.movilbox.testsantiagoalvarez.api;

import androidx.lifecycle.LiveData;

import com.movilbox.testsantiagoalvarez.model.Post;
import com.movilbox.testsantiagoalvarez.model.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface WebServiceApi {

    @GET("posts")
    LiveData<ApiResponse<List<Post>>> getAllPost();

    @GET("users/{userId}")
    LiveData<ApiResponse<User>> getUserById(@Path("userId") Integer userId);
}
