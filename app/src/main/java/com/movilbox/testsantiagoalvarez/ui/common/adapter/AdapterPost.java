package com.movilbox.testsantiagoalvarez.ui.common.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.movilbox.testsantiagoalvarez.R;
import com.movilbox.testsantiagoalvarez.model.Post;

import org.jetbrains.annotations.NotNull;

public class AdapterPost extends ListAdapter<Post, AdapterPost.ViewHolder> {
    private final IAdapterPost mCallback;

    public AdapterPost(IAdapterPost mCallback) {
        super(DIFF_CALLBACK);
        this.mCallback = mCallback;
    }


    private static final DiffUtil.ItemCallback<Post> DIFF_CALLBACK = new DiffUtil.ItemCallback<Post>() {
        @Override
        public boolean areItemsTheSame(@NonNull @NotNull Post oldItem, @NonNull @NotNull Post newItem) {
            return oldItem.isLike() == newItem.isLike() &&
                    oldItem.getTitle().equalsIgnoreCase(newItem.getTitle()) &&
                    oldItem.getBody().equalsIgnoreCase(newItem.getBody()) &&
                    oldItem.isUnread() == newItem.isUnread();
        }

        @Override
        public boolean areContentsTheSame(@NonNull @NotNull Post oldItem, @NonNull @NotNull Post newItem) {
            return oldItem.isLike() == newItem.isLike() &&
                    oldItem.getTitle().equalsIgnoreCase(newItem.getTitle()) &&
                    oldItem.getBody().equalsIgnoreCase(newItem.getBody()) &&
                    oldItem.isUnread() == newItem.isUnread();
        }
    };


    @NonNull
    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_post, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, int position) {
        holder.title.setText(getItem(position).getTitle());
        holder.body.setText(getItem(position).getBody());


        holder.like.setImageDrawable(getItem(position).isLike() ?
                ContextCompat.getDrawable(holder.itemView.getContext(), R.drawable.ic_like) :
                ContextCompat.getDrawable(holder.itemView.getContext(), R.drawable.ic_unlike));

        holder.unread.setVisibility(getItem(position).isUnread() ? View.VISIBLE : View.INVISIBLE);

    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final TextView title;
        private final TextView body;
        private final ImageView like;
        private final View unread;

        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            body = itemView.findViewById(R.id.body);
            ImageButton delete = itemView.findViewById(R.id.delete);
            like = itemView.findViewById(R.id.like);
            unread = itemView.findViewById(R.id.unread);
            itemView.setOnClickListener(this);
            delete.setOnClickListener(this);
            like.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mCallback != null) {
                if (v.getId() == R.id.delete) {
                    mCallback.onItemDelete(getItem(getAdapterPosition()));
                } else if (v.getId() == R.id.like) {
                    mCallback.onItemLikeUnLike(!getItem(getAdapterPosition()).isLike(), getItem(getAdapterPosition()).getId());
                } else
                    mCallback.onItemSelected(getItem(getAdapterPosition()));
            }
        }
    }

    public interface IAdapterPost {
        void onItemSelected(Post post);

        void onItemDelete(Post post);

        void onItemLikeUnLike(boolean likeUnLike, Integer id);
    }
}
