package com.movilbox.testsantiagoalvarez.ui.home;

import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

import com.movilbox.testsantiagoalvarez.model.Post;
import com.movilbox.testsantiagoalvarez.repository.PostRepository;
import com.movilbox.testsantiagoalvarez.repository.Resource;

import java.util.List;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;

@HiltViewModel
public class HomeViewModel extends ViewModel {

    private final LiveData<Resource<List<Post>>> posts;
    private final MutableLiveData<Integer> retryCall = new MutableLiveData<>(0);


    private final PostRepository postRepository;

    @Inject
    public HomeViewModel(PostRepository postRepository) {
        this.postRepository = postRepository;
        posts = Transformations.switchMap(retryCall, new Function<Integer, LiveData<Resource<List<Post>>>>() {
            @Override
            public LiveData<Resource<List<Post>>> apply(Integer input) {
                if (input.equals(0))
                    return postRepository.getAllPost();
                else if (input.equals(1))
                    return postRepository.getAllPostFilter(true);
                else
                    return postRepository.getAllPostFilter(false);
            }
        });

    }

    public LiveData<Resource<List<Post>>> getPosts() {
        return posts;
    }

    public void deletePost(Post post) {
        postRepository.deletePost(post);
    }

    public void onItemLikeUnLike(boolean likeUnLike, Integer id) {
        postRepository.likeUnLikePost(likeUnLike, id);
    }

    public void retryCall(Integer filter) {
        retryCall.setValue(filter);
    }

    public LiveData<Integer> getRetryCall() {
        return retryCall;
    }

    public void deleteAllPost() {
        postRepository.deleteAll();
    }

    public void setReadPost(Integer id) {
        postRepository.setRead(id);
    }
}