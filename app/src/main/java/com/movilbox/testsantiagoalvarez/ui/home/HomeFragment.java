package com.movilbox.testsantiagoalvarez.ui.home;

import android.graphics.Canvas;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.movilbox.testsantiagoalvarez.R;
import com.movilbox.testsantiagoalvarez.databinding.FragmentHomeBinding;
import com.movilbox.testsantiagoalvarez.model.Post;
import com.movilbox.testsantiagoalvarez.ui.common.adapter.AdapterPost;
import com.movilbox.testsantiagoalvarez.viewmodel.ViewModelFactory;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import dagger.hilt.android.WithFragmentBindings;

@WithFragmentBindings
@AndroidEntryPoint
public class HomeFragment extends Fragment implements AdapterPost.IAdapterPost {


    @Inject
    ViewModelFactory mViewModelFactory;

    private HomeViewModel homeViewModel;
    private FragmentHomeBinding binding;
    private AdapterPost adapter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                new ViewModelProvider(this, mViewModelFactory).get(HomeViewModel.class);


        binding = FragmentHomeBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        final RecyclerView recyclerView = binding.recyclerView;
        recyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));
        adapter = new AdapterPost(this);
        recyclerView.setAdapter(adapter);

        homeViewModel.getPosts().observe(getViewLifecycleOwner(), posts -> {


            if (posts.data == null || posts.data.isEmpty())
                binding.deleteAll.setVisibility(View.GONE);
            else {
                binding.deleteAll.setVisibility(View.VISIBLE);
                binding.swipeRefresh.setRefreshing(false);
            }
            adapter.submitList(posts.data);

        });

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT | ItemTouchHelper.LEFT) {
            private final ColorDrawable background = new ColorDrawable(R.attr.colorSecondary);

            @Override
            public boolean onMove(@NonNull @NotNull RecyclerView recyclerView, @NonNull @NotNull RecyclerView.ViewHolder viewHolder, @NonNull @NotNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull @NotNull RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();
                homeViewModel.deletePost(adapter.getCurrentList().get(position));
            }

            @Override
            public void onChildDraw(@NonNull @NotNull Canvas c, @NonNull @NotNull RecyclerView recyclerView, @NonNull @NotNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
                View itemView = viewHolder.itemView;
                int backgroundCornerOffset = 100;

                if (dX > 0) { // Swiping to the right
                    background.setBounds(itemView.getLeft(), itemView.getTop(),
                            itemView.getLeft() + ((int) dX) + backgroundCornerOffset,
                            itemView.getBottom());

                } else if (dX < 0) { // Swiping to the left
                    background.setBounds(itemView.getRight() + ((int) dX) - backgroundCornerOffset,
                            itemView.getTop(), itemView.getRight(), itemView.getBottom());
                } else { // view is unSwiped
                    background.setBounds(0, 0, 0, 0);
                }
                background.draw(c);

            }
        }).attachToRecyclerView(binding.recyclerView);


        binding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    binding.deleteAll.setText(R.string.abbv_del);
                } else {
                    binding.deleteAll.setText(R.string.delete_all);
                }
            }

        });

        homeViewModel.getRetryCall().observe(getViewLifecycleOwner(), filter -> {

            binding.like.setVisibility(filter.equals(1) ? View.GONE : View.VISIBLE);
            binding.unLike.setVisibility(filter.equals(2) ? View.GONE : View.VISIBLE);

        });

        binding.deleteAll.setOnClickListener(v -> homeViewModel.deleteAllPost());

        binding.swipeRefresh.setOnRefreshListener(() -> {
            homeViewModel.retryCall(homeViewModel.getRetryCall().getValue());
            binding.swipeRefresh.setRefreshing(false);
        });

        binding.like.setOnClickListener(v -> homeViewModel.retryCall(1));
        binding.unLike.setOnClickListener(v -> homeViewModel.retryCall(2));

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }


    @Override
    public void onItemSelected(Post post) {
        homeViewModel.setReadPost(post.getId());

        HomeFragmentDirections.ActionNavigationHomeToMapDetailFragment action =
                HomeFragmentDirections.actionNavigationHomeToMapDetailFragment();

        action.setPost(post);

        NavController navController = Navigation.findNavController(binding.getRoot());
        navController.navigate(action);

    }

    @Override
    public void onItemDelete(Post post) {
        homeViewModel.deletePost(post);
    }

    @Override
    public void onItemLikeUnLike(boolean likeUnLike, Integer id) {
        homeViewModel.onItemLikeUnLike(likeUnLike, id);
    }
}