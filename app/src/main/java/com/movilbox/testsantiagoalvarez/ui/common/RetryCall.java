package com.movilbox.testsantiagoalvarez.ui.common;

public interface RetryCall {
    void retry();
}
