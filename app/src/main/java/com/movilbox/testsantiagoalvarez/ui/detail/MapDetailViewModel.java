package com.movilbox.testsantiagoalvarez.ui.detail;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

import com.google.android.gms.maps.model.LatLng;
import com.movilbox.testsantiagoalvarez.model.Post;
import com.movilbox.testsantiagoalvarez.model.User;
import com.movilbox.testsantiagoalvarez.repository.Resource;
import com.movilbox.testsantiagoalvarez.repository.UserRepository;
import com.movilbox.testsantiagoalvarez.utils.AbsentLiveData;

import java.util.Objects;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;

@HiltViewModel
public class MapDetailViewModel extends ViewModel {

    final MutableLiveData<Integer> userId = new MutableLiveData<>();
    private final LiveData<Resource<User>> user;


    private final MutableLiveData<LatLng> latLng = new MutableLiveData<>(null);
    private Post post;

    @Inject
    public MapDetailViewModel(UserRepository userRepository) {
        user = Transformations.switchMap(userId, uID -> {
            if (uID == null) {
                return AbsentLiveData.create();
            } else {
                return userRepository.getUserById(uID);
            }
        });


    }

    public void setUserId(Integer userId) {
        if (Objects.equals(this.userId.getValue(), userId)) {
            return;
        }
        this.userId.setValue(userId);
    }

    public MutableLiveData<LatLng> getLatLng() {
        return latLng;
    }

    public LiveData<Resource<User>> getUser() {
        return user;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }


    public void setLatLng() {
        if (user.getValue() == null) return;
        latLng.postValue(new LatLng(
                Double.parseDouble(user.getValue().data.getAddress().geo.lat),
                Double.parseDouble(user.getValue().data.getAddress().geo.lat)));
    }
}
