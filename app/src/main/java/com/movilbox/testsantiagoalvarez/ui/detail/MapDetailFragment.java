package com.movilbox.testsantiagoalvarez.ui.detail;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.MarkerOptions;
import com.movilbox.testsantiagoalvarez.R;
import com.movilbox.testsantiagoalvarez.databinding.FragmentMapDetailBinding;
import com.movilbox.testsantiagoalvarez.model.Post;
import com.movilbox.testsantiagoalvarez.viewmodel.ViewModelFactory;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import dagger.hilt.android.WithFragmentBindings;

@WithFragmentBindings
@AndroidEntryPoint
public class MapDetailFragment extends Fragment {

    private final OnMapReadyCallback callback = new OnMapReadyCallback() {

        /**
         * Manipulates the map once available.
         * This callback is triggered when the map is ready to be used.
         * This is where we can add markers or lines, add listeners or move the camera.
         * In this case, we just add a marker near Sydney, Australia.
         * If Google Play services is not installed on the device, the user will be prompted to
         * install it inside the SupportMapFragment. This method will only be triggered once the
         * user has installed Google Play services and returned to the app.
         */
        @Override
        public void onMapReady(@NotNull GoogleMap googleMap) {

            viewModel.getLatLng().observe(getViewLifecycleOwner(), latLng -> {
                if (latLng == null) return;
                if (viewModel.getUser().getValue() != null)
                    googleMap.addMarker(new MarkerOptions().position(latLng).title(viewModel.getUser().getValue().data.getName()));

                googleMap.animateCamera(
                        CameraUpdateFactory
                                .newLatLngZoom(latLng, 10), 1500, null);
            });

            googleMap.setMaxZoomPreference(18);
            googleMap.setMinZoomPreference(4);
        }
    };

    private FragmentMapDetailBinding binding;
    private MapDetailViewModel viewModel;

    @Inject
    ViewModelFactory mViewModelFactory;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        viewModel =
                new ViewModelProvider(this, mViewModelFactory).get(MapDetailViewModel.class);
        binding = FragmentMapDetailBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        viewModel.getUser().observe(getViewLifecycleOwner(), user -> {
            if (user.data != null) {

                String url = "https://picsum.photos/400/400?random";


                Glide.with(binding.getRoot().getContext())
                        .load(url)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .centerCrop()
                        .into(binding.imageView);

                binding.email.setText(user.data.getEmail());
                binding.name.setText(user.data.getName());
                binding.userName.setText(user.data.getUsername());
                binding.showInMap.setEnabled(true);

            } else {
                binding.showInMap.setEnabled(false);
            }
        });

        if (getArguments() != null) {
            Post post = MapDetailFragmentArgs.fromBundle(getArguments()).getPost();
            if (post != null) {
                viewModel.setPost(post);
                viewModel.setUserId(post.getUserId());
            }
        }

        binding.showInMap.setOnClickListener(v ->
        {
            viewModel.setLatLng();
        });


        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SupportMapFragment mapFragment =
                (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(callback);
        }
    }
}